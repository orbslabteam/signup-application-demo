<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="This is a e-dairy for students"/>
<meta name="keywords" content="e-dairy, student dairy"/>
<meta name="author" content="Leading University, Sylhet"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="logstyle.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js">
<link rel="icon" href="dairy.jpg">
<title>Student Dairy</title>
</head>

<body>
<header>Student<br/><span style="display:inline-block; width: 150px;"></span>Dairy</header>
<div id="log" class="container">
  <h2>Open your Dairy</h2>
  <form role="form" action="login_success.php" method="post">
    <div class="form-group">
      <label for="email2">Email:</label>
      <input type="email" class="form-control" id="email2" placeholder="Enter email" name="email">
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pass">
      <span class="help-block" style="color:white">Please make that sure your password is correct</span>
    </div>
    <div class="checkbox">
      <label>
        <input type="checkbox">
        Remember me</label>
        &nbsp;&nbsp;&nbsp;<a href="#" style="color:rgba(47,47,47,1.00)">Forget Password</a>
    </div>
    <button type="submit" class="btn btn-default" name="submit">Submit</button>
  </form>
</div>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<div id="create" align="center">
<img src="dairycopy.gif"/><br/><br/>
<a href="create.html" target="_blank">Create Your Own Dairy</a>
</div>
</body>
</html>